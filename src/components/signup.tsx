import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import Link from "next/link";

export default function SignUpComponent(props: {
    firstNameValue: string;
    firstNameChange: (value: string) => void;
    lastNameValue: string;
    lastNameChange: (value: string) => void;
    emailValue: string;
    emailChange: (value: string) => void;
    passwordValue: string;
    passwordChange: (value: string) => void;
    login: (provider: string) => void;
    loginCredentials: () => void;
}) {
    return (
        <div className="flex items-center min-h-screen px-4 sm:px-6 lg:px-8">
            <div className="w-full max-w-md mx-auto space-y-8">
                <div className="space-y-2">
                    <h1 className="text-3xl font-bold">Sign Up</h1>
                    <p className="text-gray-500 dark:text-gray-400">
                        Enter your information to create an account
                    </p>
                </div>
                <div className="space-y-4">
                    <div className="grid grid-cols-2 gap-4">
                        <div className="space-y-2">
                            <Label htmlFor="first-name">First name</Label>
                            <Input
                                id="first-name"
                                placeholder="Axel"
                                required
                                value={props.firstNameValue}
                                onKeyDown={(e) => {
                                    if (e.key === "Enter")
                                        props.loginCredentials()
                                }}
                                onChange={(e) =>
                                    props.firstNameChange(e.target.value)
                                }
                            />
                        </div>
                        <div className="space-y-2">
                            <Label htmlFor="last-name">Last name</Label>
                            <Input
                                id="last-name"
                                placeholder="Chord"
                                required
                                value={props.lastNameValue}
                                onKeyDown={(e) => {
                                    if (e.key === "Enter")
                                        props.loginCredentials()
                                }}
                                onChange={(e) =>
                                    props.lastNameChange(e.target.value)
                                }
                            />
                        </div>
                    </div>
                    <div className="space-y-2">
                        <Label htmlFor="email">Email</Label>
                        <Input
                            id="email"
                            placeholder="tunester@mail.com"
                            required
                            type="email"
                            value={props.emailValue}
                            onKeyDown={(e) => {
                                if (e.key === "Enter")
                                    props.loginCredentials()
                            }}
                            onChange={(e) => props.emailChange(e.target.value)}
                        />
                    </div>
                    <div className="space-y-2">
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            required
                            type="password"
                            value={props.passwordValue}
                            onKeyDown={(e) => {
                                if (e.key === "Enter")
                                    props.loginCredentials()
                            }}
                            onChange={(e) =>
                                props.passwordChange(e.target.value)
                            }
                        />
                    </div>
                    <Button
                        className="w-full"
                        type="submit"
                        onClick={props.loginCredentials}
                    >
                        Sign Up
                    </Button>
                    <Separator className="my-8" />
                    <div className="space-y-4 flex flex-col items-center justify-center">
                        <Button
                            className="w-full"
                            variant="outline"
                            onClick={() => {
                                props.login("google");
                            }}
                        >
                            Sign up with Google
                        </Button>
                        <div className="flex flex-row mt-4 text-center text-sm gap-1">
                            Already have an account?
                            <Link className="underline" href="/login">
                                Login
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
