import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import Link from "next/link";
import { Button } from "@/components/ui/button";

export default function LoginComponent(props: {
    emailValue: string;
    emailChange: (value: string) => void;
    passwordValue: string;
    passwordChange: (value: string) => void;
    login: (provider: string) => void;
    loginCredentials: () => void;
}) {
    return (
        <div className="flex items-center min-h-screen px-4 sm:px-6 lg:px-8">
            <div className="w-full max-w-md mx-auto space-y-8">
                <div className="space-y-2">
                    <h1 className="text-3xl font-bold">Login</h1>
                    <p className="text-gray-500 dark:text-gray-400">
                        Enter your email below to login to your account
                    </p>
                </div>
                <div className="space-y-4">
                    <div className="space-y-2">
                        <Label htmlFor="email">Email</Label>
                        <Input
                            id="email"
                            placeholder="tunester@mail.com"
                            required
                            type="email"
                            value={props.emailValue}
                            onKeyDown={(e) => {
                                if (e.key === "Enter")
                                    props.loginCredentials()
                            }}
                            onChange={(e) => {
                                props.emailChange(e.target.value);
                            }}
                        />
                    </div>
                    <div className="space-y-2">
                        <div className="flex items-center">
                            <Label htmlFor="password">Password</Label>
                            {/* <Link
                                className="ml-auto inline-block text-sm underline"
                                href="#"
                            >
                                Forgot your password?
                            </Link> */}
                        </div>
                        <Input
                            id="password"
                            required
                            type="password"
                            value={props.passwordValue}
                            onKeyDown={(e) => {
                                if (e.key === "Enter")
                                    props.loginCredentials()
                            }}
                            onChange={(e) => {
                                props.passwordChange(e.target.value);
                            }}
                        />
                    </div>
                    <Button className="w-full" onClick={props.loginCredentials}>
                        Login
                    </Button>
                    <Button
                        className="w-full"
                        variant="outline"
                        onClick={() => {
                            props.login("google");
                        }}
                    >
                        Login with Google
                    </Button>
                </div>
                <div className="flex items-center justify-center">
                    <div className="text-center text-sm flex gap-1">
                        Don&apos;t have an account?
                        <Link className="underline" href="/signup">
                            Sign up
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}
