"use client";

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Slider } from "@/components/ui/slider";
import { invoke } from "@tauri-apps/api/tauri";
import { signOut, useSession } from "next-auth/react";
import { redirect, useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import SettingPane from "@/components/settings";
import UploadPane from "@/components/upload";
import { useTheme } from "next-themes";
import {
  Play,
  Pause,
  SkipForward,
  SkipBack,
  Repeat,
  Repeat1,
} from "lucide-react";

const useLocalToken = () => {
  const token =
    typeof window !== "undefined" && window.localStorage
      ? localStorage.getItem("token") || undefined
      : undefined;

  return token;
};

export default function Home() {
  const router = useRouter();
  const { setTheme } = useTheme();
  const [user, setUser] = useState(true);
  const [search, setSearch] = useState("");
  const localStorageToken = useLocalToken();
  const [tunes, setTunes] = useState([] as any[]);
  const [playing, setPlaying] = useState<any[]>([]);
  const [allTunes, setAllTunes] = useState([] as any[]);
  const audioPlayerRef = useRef<HTMLAudioElement>(null);
  const [currentPosition, setCurrentPosition] = useState(0);
  const [songSelected, setSongSelected] = useState<any[]>([]);
  const [userType, setUserType] = useState<string>("listener");
  const [searchedTunes, setSearchedTunes] = useState([] as any[]);
  const [displayMode, setDisplayMode] = useState<string>("explore");
  const [currentPage, setCurrentPage] = useState<string>("dashboard");
  const [currentPlayingKey, setCurrentPlayingKey] = useState<string>("");
  const [currentPlayingUrl, setCurrentPlayingUrl] = useState<string>("");
  const [createPlaylistPopover, setCreatePlaylistPopover] = useState(false);

  const { data: session } = useSession();
  async function getCurrentTunes() {
    const tunes = await invoke("get_tunes");
    setAllTunes(tunes as any[]);
    setTunes(tunes as any[]);
  }

  useEffect(() => {
    getCurrentTunes();
    const tunesLength = allTunes.length;
    setSongSelected(Array(tunesLength).fill(false));
    setPlaying(Array(tunesLength).fill(false));
  }, []);

  useEffect(() => {
    getCurrentTunes();
  }, [currentPage]);

  useEffect(() => {
    if (audioPlayerRef.current) {
      audioPlayerRef.current.load();
    }

    if (playing.length > 0) {
      const idx = playing.findIndex((play) => play === true);
      if (idx !== -1 && audioPlayerRef.current) {
        audioPlayerRef.current.play();
      }
    }

    return () => {
      if (audioPlayerRef.current) {
        audioPlayerRef.current.pause();
      }
    };
  }, [currentPlayingUrl]);

  useEffect(() => {
    if (displayMode === "search") {
      setTunes(searchedTunes);
    } else {
      setTunes(allTunes);
    }
  }, [displayMode, searchedTunes]);

  useEffect(() => {
    async function getCloudfrontUrl() {
      const key = currentPlayingKey;
      const url: string = await invoke("get_cloudfront_url", { key: key });
      setCurrentPlayingUrl(url);
    }

    getCloudfrontUrl();
  }, [currentPlayingKey]);

  useEffect(() => {
    invoke("verify_token", { token: localStorageToken })
      .then((_) => {})
      .catch((_) => {
        setUser(false);
        if (!session) router.push("/login");
      });

    if (!session && !user) redirect("/login");
  }, [session, localStorageToken, router, user]);

  useEffect(() => {
    const audioPlayer = audioPlayerRef.current;

    const updateTime = () => {
      if (audioPlayer) {
        const currentTime = audioPlayer.currentTime;
        const duration = audioPlayer.duration;
        const percentage = (currentTime / duration) * 100;
        setCurrentPosition(percentage);
      }
    };

    audioPlayer?.addEventListener("timeupdate", updateTime);

    return () => {
      if (audioPlayer)
        audioPlayer.removeEventListener("timeupdate", updateTime);
    };
  }, [audioPlayerRef]);

  const togglePage = (page: string) => {
    setCurrentPage(page);
  };

  const logoutHadler = async () => {
    localStorage.removeItem("token");
    await signOut();
  };

  const deleteTune = async () => {
    await invoke("remove_tune", { key: search });
  };

  const searchHandler = async () => {
    if (search === "") {
      setTunes(allTunes);
      return;
    }

    setDisplayModeUtil("search");
    setSearchedTunes(
      tunes.filter((tune) => {
        const searchTerm = search.toLowerCase();
        const albumName = tune.album_name.toLowerCase();
        const tuneTitle = tune.song_title.toLowerCase();
        const artistName = tune.artist_name.toLowerCase();
        return (
          tuneTitle.includes(searchTerm) ||
          artistName.includes(searchTerm) ||
          albumName.includes(searchTerm)
        );
      }),
    );
  };

  const setDisplayModeUtil = (mode: string) => {
    setDisplayMode(mode);
  };

  const createPlaylistContainer = (
    <div className="flex flex-col gap-5 items-center justify-center">
      <Input
        type="text"
        placeholder="Playlist Name"
        className="rounded-full h-[3rem]"
      />
      <div className="flex flex-row gap-5">
        <Button className="rounded-full">Create</Button>
        <Button
          className="rounded-full"
          variant="secondary"
          onClick={() => {
            setCreatePlaylistPopover(false);
          }}
        >
          Cancel
        </Button>
      </div>
    </div>
  );

  return (
    <main className="flex flex-row text-base justify-between items-center w-screen h-screen">
      {currentPage === "settings" && (
        <SettingPane darkModeToggler={setTheme} setUserType={setUserType} />
      )}
      {currentPage === "upload" && <UploadPane />}
      {/* todo: change to grid layout */}
      {currentPage === "dashboard" && (
        <div
          id="mainSection"
          className="flex flex-col h-[90%] w-screen overflow-y-auto items-center justify-start self-start"
        >
          <div className="flex flex-row w-full h-[10%] items-center justify-start border border-500-yellow">
            <span className="flex text-sm font-semibold m-5 p-5 cursor-default min-w-[10%]">
              #
            </span>
            <span className="flex text-sm font-semibold m-5 p-5 cursor-default min-w-[17%] justify-center">
              Title
            </span>
            <span className="flex text-sm font-semibold m-5 p-5 cursor-default min-w-[17%] justify-center">
              Artist
            </span>
            <span className="flex text-sm font-semibold m-5 p-5 cursor-default min-w-[17%] justify-center">
              Album
            </span>
            <span className="text-sm font-semibold m-5 p-5 cursor-default min-w-[17%] justify-center hidden xl:flex">
              Duration
            </span>
          </div>
          {tunes.map((tune, idx) => {
            return (
              <div
                key={tune.key}
                // make the minimum size of div not less than 10% of the screen
                className="flex flex-row w-full h-[10%] items-center justify-start border border-500-yellow"
                onMouseEnter={() => {
                  setSongSelected((prev) => {
                    const newSelected = [...prev];
                    newSelected[idx] = true;
                    return newSelected;
                  });
                }}
                onMouseLeave={() => {
                  setSongSelected((prev) => {
                    const newSelected = [...prev];
                    newSelected[idx] = false;
                    return newSelected;
                  });
                }}
              >
                <span className="flex text-lg font-light m-5 p-5 cursor-default min-w-[10%]">
                  {songSelected[idx] ? (
                    <Play
                      className="h-[1rem] w-[1rem] cursor-pointer"
                      onClick={() => {
                        setPlaying((prev) => {
                          const newPlaying = [...prev];
                          newPlaying.fill(false);
                          newPlaying[idx] = true;
                          return newPlaying;
                        });
                        setCurrentPlayingKey(tune.key);
                      }}
                    />
                  ) : (
                    idx + 1
                  )}
                </span>
                <span className="flex text-[1rem] m-5 p-5 cursor-default min-w-[17%] justify-center items-center">
                  <span className="truncate">{tune.song_title}</span>
                </span>
                <span className="flex text-[1rem] m-5 p-5 cursor-default min-w-[17%] justify-center items-center">
                  <span className="truncate">{tune.artist_name}</span>
                </span>
                <span className="flex text-[1rem] m-5 p-5 cursor-default min-w-[17%] justify-center items-center">
                  <span className="truncate">
                    {tune.album_name.length === 0 ? "-" : tune.album_name}
                  </span>
                </span>
                <span className="flex text-[1rem] m-5 p-5 cursor-default min-w-[17%] justify-center items-center">
                  <span className="truncate">
                    {tune.duration.charAt(0) === "0"
                      ? tune.duration.slice(2)
                      : tune.duration}
                  </span>
                </span>
              </div>
            );
          })}
          {/* <div
                            id="musicContent"
                            className="flex w-[75%] flex-col h-max items-center justify-center"
                        >
                        </div> */}
        </div>
      )}
      {(currentPage === "dashboard" ||
        currentPage === "settings" ||
        currentPage === "upload") && (
        <div
          id="mediaControls"
          className="flex flex-col absolute md:w-[100%] lg:w-[82.5%] h-[10%] items-center justify-center bottom-[0%] z-10 left-[0%] gap-3 self-center p-10 bg-inherit"
        >
          <div className="hidden w-full items-center justify-center">
            <audio
              controls
              autoPlay
              className="flex w-full"
              ref={audioPlayerRef}
            >
              <source src={currentPlayingUrl} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
          </div>

          <div className="flex flex-row gap-14 w-1/2 items-center justify-center">
            <Button className="flex rounded-full h-[3rem] w-[3rem]">
              <SkipBack className="h-[1.5rem] w-[1.5rem]" />
            </Button>
            {playing && songSelected !== undefined ? (
              <Button
                className="flex rounded-full h-[3rem] w-[3rem]"
                onClick={() => {
                  // audioPlayerRef.current?.pause();
                  setSongSelected((prev) => {
                    const newSelected = [...prev];
                    newSelected.fill(false);
                    return newSelected;
                  });
                }}
              >
                <Pause className="h-[1.5rem] w-[1.5rem]" />
              </Button>
            ) : (
              <Button
                className="rounded-full h-[3rem] w-[3rem]"
                onClick={() => {
                  // audioPlayerRef.current?.play()
                  setSongSelected((prev) => {
                    const newSelected = [...prev];
                    newSelected.fill(false);
                    return newSelected;
                  });
                }}
              >
                <Play className="h-[1.5rem] w-[1.5rem]" />
              </Button>
            )}
            <Button className="rounded-full h-[3rem] w-[3rem]">
              <SkipForward className="h-[1.5rem] w-[1.5rem]" />
            </Button>
            <Button className="rounded-full h-[3rem] w-[3rem]">
              <Repeat className="h-[1.5rem] w-[1.5rem]" />
            </Button>
            <Button className="rounded-full h-[3rem] w-[3rem]">
              <Repeat1 className="h-[1.5rem] w-[1.5rem]" />
            </Button>
          </div>
          <Slider
            defaultValue={[0]}
            value={[currentPosition]}
            max={100}
            step={1}
            className="bg-inherit mt-3"
          />
        </div>
      )}
      <div
        id="sideBar"
        className="relative items-center justify-start hidden lg:flex w-96 h-full flex-col gap-5 border border-600-grey rounded-sm border-t-0 border-b-0"
      >
        <span className="opacity-60 text-[5rem] font-extralight m-10 p-10 pb-0 cursor-default">
          Tunesy
        </span>
        <div className="flex w-full max-w-sm items-center space-x-2 p-5 ml-10 mr-10">
          <Input
            type="text"
            onChange={(e) => {
              setSearch(e.target.value);
              searchHandler();
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                searchHandler();
              }
            }}
            value={search}
            placeholder="Search"
            className="rounded-full h-[3.75rem] p-3"
          />
          <Button
            type="submit"
            className="rounded-full"
            onClick={searchHandler}
          >
            Go
          </Button>
        </div>
        <div className="flex flex-col gap-5 w-full">
          <ul className="flex-col">
            <li>
              <Button
                variant="outline"
                className="rounded-none w-full h-[5.5rem]"
                onClick={() => {
                  togglePage("dashboard");
                  setDisplayModeUtil("explore");
                }}
              >
                Explore
              </Button>
            </li>
            <li>
              <Button
                variant="outline"
                className="rounded-none w-full h-[5.5rem]"
              >
                Liked Songs
              </Button>
            </li>
            <li>
              <Button
                variant="outline"
                className="rounded-none w-full h-[5.5rem]"
                onClick={() => {
                  togglePage("settings");
                }}
              >
                Settings
              </Button>
            </li>
            {userType === "artist" && (
              <li>
                <Button
                  variant="outline"
                  className="rounded-none w-full h-[5.5rem]"
                  onClick={() => {
                    togglePage("upload");
                  }}
                >
                  Upload
                </Button>
              </li>
            )}
          </ul>
        </div>
        {createPlaylistPopover && (
          <div className="absolute bottom-0 border border-500-grey w-full p-10 justify-self-end border-r-0 border-l-0 border-b-0 rounded-[3rem]">
            {createPlaylistContainer}
          </div>
        )}
        {!createPlaylistPopover && (
          <Button
            type="submit"
            className="rounded-full absolute bottom-[5%]"
            onClick={() => {
              // setCreatePlaylistPopover(true);
              logoutHadler();
            }}
          >
            Create Playlist
          </Button>
        )}
      </div>
    </main>
  );
}
