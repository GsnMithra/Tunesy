use bcrypt::{hash, verify};
use std::env;

pub async fn hash_password(password: String) -> String {
    let salt = env::var("HASH_SALT").unwrap().parse::<u32>().unwrap();
    hash(password, salt).unwrap()
}

pub async fn verify_password(password: String, hash: String) -> bool {
    verify(password, &hash).unwrap()
}
