#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
mod utils;

use utils::{
    add_tune, add_user, delete_user, get_cloudfront_url, get_tunes, login_user, remove_tune,
    verify_token,
};

use dotenv::dotenv;

#[tokio::main]
async fn main() {
    dotenv().ok();

    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            get_tunes,
            add_tune,
            remove_tune,
            add_user,
            delete_user,
            login_user,
            verify_token,
            get_cloudfront_url
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
